# Algorithmic Design Exam Repository

Note: all algorithms are stored in the headers (except for string matching).
The .cpp files are used mainly for testing & debugging and show examples of how the classes may be used.
Compile .cpp files with `g++ -std=c++11 -Wno-narrowing classname.cpp`

Algorithms moved to "algo" folder

Algs list:
- Vectors sorting and selection -> all algorithms inside `Vector.hh`
  - Insertion sorting
  - Select
- Matrix multiplication -> all algorithms inside `strassen.hh`
  - Strassen algorithm
- Non weighted graphs -> all algorithms inside `Graph.hh`
  - BFS
  - DFS
  - Tarjan
  - Fischer-Meyer *last step does not seem to work*
- Weighted graphs -> all algorithms inside `Wgraph.hh`
  - Dijkstra
  - A*
  - Floyd Warshall
- String matching -> all algorithms inside `Strings.cpp`
  - Knut Morris Pratt
  