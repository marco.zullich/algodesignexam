#ifndef VECTORHH
#define VECTORHH

#include <iostream>
#include <stdexcept>
#include <functional>
#include <cmath>
#include <utility>

template <typename T>
class Vector{
protected:
	//Size, space and elements
	unsigned int n;
	unsigned int s;
	T* elem;
	
public:
	Vector(unsigned int sz=0, T default_elem= T{}): n{sz}, elem{new T[n]}, s{sz} {
		for(unsigned int i=0;i<sz;++i){
			elem[i]=default_elem;
		}
	}
	~Vector() {delete[] elem;}

	//copy semantics
	Vector(const Vector& v): n{v.size()}, elem{new T[n]}, s{v.space()} {
		for(int i=0;i<n;++i){
			elem[i] = v[i];
		}
	}

	Vector& operator=(const Vector& v){

		if(this==&v) return *this;

		//this vector has already enough space to store all elements of v
		if(v.size()<=space()){
			for(int i=0;i<v.size();++i){
				elem[i] = v.elem[i];
			}
			n = v.size();
			return *this;
		}

		//this vector has not enough space...
		T* temp = new T[v.size()];
		for(int i=0;i<n;++i){
			temp[i] = v[i];
		}
		delete[] elem;
		elem = temp;
		n = s = v.size();
		return *this;
	}

	//move semantics
	Vector(Vector&& v): n{v.size()}, elem{v.elem}, s{v.space()} {
		v.n = 0;
		v.s = 0;
		v.elem = nullptr;
	}

	Vector& operator=(Vector&& v){
		delete[] elem;
		elem = v.elem;
		n = v.size();
		s = v.space();
		v.elem = nullptr;
		v.n = 0;
		v.s = 0;
		return *this;
	}

	bool isEmpty(){
		return n==0;
	}

	bool operator==(Vector<T>& v){
		if(v.size()!=size()){
			return false;
		}
		if(v.isEmpty() & isEmpty()){
			return true;
		}
		for(unsigned int i = 0; i<size(); ++i){
			if(elem[i]!=v[i]){
				return false;
			}
		}
		return true;
	}

	T& operator[](const unsigned int i){return elem[rangecheck(i)];}
	const T& operator[](const unsigned int i) const {return elem[rangecheck(i)];}

	const unsigned int rangecheck(const unsigned int i) const {if(i>=n) throw std::runtime_error ("Array index out of bounds"); return i;}

	const unsigned int size() const {return n;}

	const unsigned int space() const {return s;}

	void reserve(const unsigned int newalloc){
		T* temp = new T*[newalloc];

		//Handles cases in which newalloc is smaller than oldalloc
		const unsigned int lim;
		if(newalloc > size()) lim = size();
		else if(newalloc < size()) lim = newalloc;
		else return; //no need for modifications if newalloc==oldsize

		for(int i=0; i<lim; ++i){
			temp[i] = elem[i];
		}
		delete[] elem;

		elem = temp;
		n = newalloc;
	}

	

	void resize(const unsigned int newsize){
		reserve(newsize);
		n = newsize;
		//assume that all vector elems are already initialized to their default
	}

	void push_back(T t){
		if(size()==0) reserve(8);
		else if (size()==space()) reserve(2*space());
		elem[n] = t;
		++n;
	}

	//insertion sort handling both ascending and descending case
	void sort(int begin=0, int end=0){
		if(end==0){end = size();}
		if(size()==1){
			return;
		}
		for(int j=begin+1; j<end; ++j ){
			T k = elem[j];
			int i = j-1;
			//Note - condition on i>0 was posponed inside the loop, because it yields error
			//when trying to access elem[-1]
			//generally speaking -> -1 becomes begin -1
			while(elem[i]>k){
				elem[i+1] = elem[i];
				--i;
				if(i==begin-1){
					break;
				}
			}
			elem[i+1] = k;
		}
	}

	//insertion sorting wrt an external vector and an ordering function (which may be passed by the user)
	void sortBy(Vector<int> ext, std::function<bool (int,int)> comp = std::greater<int>{}){
		if(ext.size()!=size()){
			throw std::runtime_error("The arrays to be ordered and the ordering array need to have the same size.");
		}
		if(size()==1){
			return;
		}
		//Note - to work, it needs to sort the external array as well
		//we need hence to sort a deep copy to avoid touching the original array
		Vector<int> ext_copy {ext};

		for(int j=1; j<size(); ++j ){
			T k = elem[j];
			int k2 = ext_copy[j];
			int i = j-1;
			while(comp(ext_copy[i],k2)){
				elem[i+1] = elem[i];
				ext_copy[i+1] = ext_copy[i];
				--i;
				if(i==-1){
					break;
				}
			}
			elem[i+1] = k;
			ext_copy[i+1] = k2;
		}
	}



private:
	T select_pivot(Vector<T>& v, unsigned int begin, unsigned int end){
		//get number of medians
		unsigned int nob = ceil((end-begin)/5.0);
		//create empty array of medians
		Vector<T> medians {nob};

		//for each median
		for(unsigned int i=0; i<nob; ++i){
			//check group of 5 elems
			unsigned int cbegin = begin + 5*i;
			//next piece of code ensures we actually have 5 elems at disposal
			//useful if array size isn't multiple of 5...
			unsigned int cend{end-cbegin};
			unsigned int median_index{};
			switch(cend){
				case 1:	median_index=0; break;
				case 2: median_index=1; break;
				default: median_index=2; break;
			}
			//regularize cend - if larger than 5, just set it to cbegin + 5
			if(cend>5){cend=cbegin+5;}
			//otherwise, it'll let us stop the array traversing before going out of bounds.

			v.sort(cbegin, cend);
			medians[i] = v[cbegin+median_index];
		}

		//repeat inner select with medians array, this time selecting the median of the medians
		unsigned int z = nob/2;

		select_inner(medians,z,0,nob);
		return medians[z];
	}



	std::pair<unsigned int, unsigned int> tri_partition(Vector<T>& v, unsigned int begin, unsigned int end, T& p){
		//piv_index stores the index of the pivot in the array
		int piv_index = end;

		//save placeholders:
		//greater saves number of elements in array which are greater than pivot_orig
		unsigned int greater {0};
		//equalse saves number ... equal than - molteplicity of the pivot element
		unsigned int equals {0};


		//start traversing the array
		for(unsigned int i=0; i<end-greater; ++i){
			
			//first possibility: if current element larger than pivot...
			if(v[i]>p){
				//swap with last place element in array (minus greater)
				T temp = v[end-greater-1];
				v[end-greater-1]=v[i];
				v[i]=temp;
				//increase "greater", so in next i's I won't be touching this elem anymore
				++greater;
				//decrease i, since now the elem at position i hasn't been visited yet
				--i;
			//second possibility: if current element smaller than pivot AND I have already encountered a pivot in the arr...
			}else if(v[i]<p & i>piv_index){
				//just swap it with pivot, minus its molteplicities
				//actually, what's happening is that the visited pivot's molteplicity with smaller position
				//in the array gets swapped with the actual smaller element
				T temp = v[piv_index];
				v[piv_index]=v[i];
				v[i]=temp;
				//since pivot has been swapped, update index
				++piv_index;
			}else if(v[i]==p){
				//if the actual element is a molteplicity of the pivot, just increase the molteplicity
				++equals;
				//if the pivot hasn't been encountered yet, just update piv_index
				if(piv_index >= end){
					piv_index = i;
				}
			}			
		}

		//the return pair is a couple of ints, storing the first occurrence of the pivot and its last
		//(index + multeplicities - 1)
		return std::make_pair(piv_index, piv_index+equals-1);

	}

	//select_inner returns void because it already sorts the array - at least the partition we're interested in
	//and hence we don't need to return anything, we'll just call v[j] in outer method
	void select_inner(Vector<T>& v, unsigned int& j, unsigned int begin = 0, unsigned int end = 0){
		//regularize end to size of v
		if(end==0){
			end = v.size();
		}

		//if size smaller than 140, use insertion sort ("sort" here)
		if(end-begin<140){
			v.sort(begin, end);
			return;
		}

		T piv = select_pivot(v, begin, end);

		std::pair<unsigned int, unsigned int> k1k2 = tri_partition(v, begin, end, piv);

		if(j<k1k2.first){
			select_inner(v,j,begin,k1k2.first-1);
		}
		if(j>k1k2.second){
			select_inner(v,j,k1k2.second+1,end);
		}

		return;
	}
 
public:
	T select(unsigned int j){
		//Create deep copy - don't want to mess up ordering on this array!
		Vector<T> this_copy {*this};

		//Call inner private algo
		select_inner(this_copy, j);
		
		return this_copy[j];

	}

	
};



//Range Python-style
Vector<int> range(int start, int end){
	if(start>=end){
		throw std::runtime_error("In range, start must be smaller than end");
	}
	Vector<int> rng{end-start};
	for(int i=0; i<end-start; ++i){
		rng[i] = i;
	}
	return rng;
}

//printing utility
template <typename T>
std::ostream& operator<<(std::ostream& os, const Vector<T>& arr) {
	os << "[";
	for (auto i = 0; i < arr.size(); ++i) {
		os <<  arr[i] << ", ";
	}
	os << "\b\b]" << std::endl;
  return os;
}

#endif