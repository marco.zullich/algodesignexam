#include "LList.hh"

LList<int> dummy(){
        LList<int> a;
        a.enqueue(11);
        return a;
    }

int main(){
    
    LList<int> l{};

    l.enqueue(0);
    l.enqueue(4),
    l.enqueue(8);
    std::cout << l;

    l.insert_as_heap(-1);
    l.insert_as_heap(2);
    l.insert_as_heap(6);

    std::cout << l;

    l.remove(0);

    std::cout << l;

    l.update(4,5);

    l.update(-1,3);

    std::cout << l;

    return 1;
}

