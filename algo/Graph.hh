#ifndef GRAPHHH
#define GRAPHHH


#include<iostream>
#include<algorithm>
#include<cmath>
#include<utility>
#include "LList.hh"
#include "Vector.hh"
#include "SquareMatrix.hh"
#include "strassen.hh"


//Note: Graph is built by passing an adj. list, here a vector of linked lists, one for each node

//Define color for algs
enum class Color {white, gray, black};

class Graph{
protected:
    Vector<LList<int>> adjList;


public:

    struct Node{
        Color c;
        int d; //Distance
        int p; //Parent
        int f; //Time
        int l; //lowlink

        //In the constructor we set defaults - Notes:
            //distance = -1 -> is infty
            //parent = -1 -> is null
            //lowlink = -1 -> is infty
        Node(Color color = Color::white, int distance=-1, int parent=-1, int time=0, int lowlink=-1): c{color}, d{distance}, p{parent}, f{time}, l{lowlink} {}
    };

    Graph(Vector<LList<int>> adjacencyList): adjList{adjacencyList} {}

    Graph(const Graph& g): adjList{g.adjList} {} 

    Graph(Graph&& g): adjList{std::move(g.adjList)} {}

    //returns no. of nodes
    const unsigned int getNodeSize() const {return adjList.size();}

    //returns all edges departing from given node
    const LList<int> getEdges (const unsigned int i) const {return adjList[i];}

    //transposes the graph
    Graph transpose(){
        Vector<LList<int>> adjT {getNodeSize()};

        for(int i=0; i<getNodeSize();++i){
            for(auto v : adjList[i]){
                adjT[v].enqueue(i);
            }
        }

        return Graph{adjT};
    }

    //Breadth First Search
    Vector<Node> BFS(int start){
        //Create vector of nodes to store results
        Vector<Node> nodes{getNodeSize()};
        //initialize first node - other nodes are already initialized in previous instruction
        nodes[start] = Node(Color::gray, 0);

        //Create empty queue (List)
        LList<int> q;
        //Add first node to the queue
        q.enqueue(start);

        //run core of the algo
        while(!q.isEmpty()){
            //pop and store value
            int u = q.dequeue();
            //visit adjacency of u
            for(auto v : getEdges(u)){
                //update node values for each "new" vertex linked to u
                if(nodes[v].c == Color::white){
                    nodes[v].c = Color::gray;
                    nodes[v].d = nodes[u].d + 1;
                    nodes[v].p = u;
                    //enqueue node v
                    q.enqueue(v);
                }
            }
            nodes[u].c = Color::black;
        }

        return nodes;

    }

    //Depth First Search --first define depth first visit
    //NOTE: it's thought so we can compute both DFS and Topo Sorting
    //we store an index k meant to populate correctly - end to start - topsort array, like a push in a queue
private:
    void DFV(int& u, int& k, Vector<Node>& nodes, Vector<int>& topSort, unsigned int& time){
        ++time;
        nodes[u].d = time;
        nodes[u].c = Color::gray;

        for(auto v : getEdges(u)){
            if(nodes[v].c == Color::white){
                nodes[v].p = u;
                //call recursively same method
                DFV(v, k, nodes, topSort, time);
            }
        }
        nodes[u].c = Color::black;
        ++time;

        ///DFS part
        nodes[u].f = time;
        ///topo sorting part
        topSort[getNodeSize()-k]=u;
        ++k; //increase array index
    }

    
    //...then search - Note: the algorithm is used also to return the vertices topologicallly sorted
public:
    std::pair<Vector<Node>, Vector<int>> DFS(){
        Vector<Node> nodes {getNodeSize()};
        Vector<int> topSort {getNodeSize()};
        unsigned int time=0;
        int k=1;
        for(int i=0; i<nodes.size(); ++i){
            if (nodes[i].c == Color::white){
                DFV(i, k, nodes, topSort, time);
            }
        }
        return std::make_pair(nodes,topSort);
    }

private:
    void Tarjan_SCC_inner(int& v, int& time, LList<int>& queue, Vector<Node>& nodes , LList<LList<int>>& sccs){
        //update node values
        nodes[v].d = time;
        nodes[v].l = time;
        nodes[v].c = Color::gray;
        ++time;
        //update queue via push
        queue.push_front(v);

        for(auto w : getEdges(v)){
            if(nodes[w].c == Color::white){
                //if node is white, call recursively and update lowlink
                Tarjan_SCC_inner(w, time, queue, nodes, sccs);
                nodes[v].l =  std::min(nodes[v].l, nodes[w].l);
            }else if(nodes[w].c == Color::gray){
                //if node is gray, only update lowlink
                nodes[v].l =  std::min(nodes[v].l, nodes[w].l);
            }
        }
        if(nodes[v].l == nodes[v].d){ //if we meet the representative of the SCC...
            LList<int> scc {};
            int z;
            //insert vertices into scc
            do{
                z = queue.dequeue();
                scc.enqueue(z);
            }while(z!=v);
            //add scc to sccs container
            sccs.enqueue(scc);
        }
    }

public:
    LList<LList<int>> Tarjan_SCC(){
        Vector<Node> nodes {getNodeSize()};
        //all nodes initialized to default by constructor

        LList<int> queue {}; //empty queue

        LList<LList<int>> sccs {}; //empty sccs container

        int time = 0;

        for(int i=0; i<getNodeSize(); ++i){
            if (nodes[i].c == Color::white){
                //Call core method
                Tarjan_SCC_inner(i, time, queue, nodes, sccs);
            }
        }

        return sccs;
    }

    SquareMatrix getAdjMatrix(){
        SquareMatrix s {getNodeSize()};

        for(int i=0;i<getNodeSize();++i){
            for(auto v : getEdges(i)){
                s(i,v) = 1;
            }
        }

        return s;
    }

    Graph collapse(LList<LList<int>> sccs){
        
        //create new ADJ LIST for new graph
        Vector<LList<int>> newAdjList {sccs.getSize()};
        
        //create map between old and new nodes
        Vector<int> V2M {getNodeSize()};
        int i {};

        //Loop into sccs - list of lists
        for(auto w : sccs){
            for(auto j : w){
                V2M[j] = i;
            }
            ++i;
        }
        
        //Populate new ADJ LIST
        for(int u=0;u<getNodeSize();++u){
            for(auto v : getEdges(u)){
                if(V2M[v]!=V2M[u]){ //← avoids loops
                    // the ",false)" avoids adding redundant elems into the list
                    newAdjList[V2M[u]].enqueue(V2M[v],false);
                }
                
            }
        }
        return Graph {newAdjList};

    }

//############################# Note: FISCHER MEYER IS STILL WORK IN PROGRESS
private:
    SquareMatrix UTMatrixTransclose_inner(SquareMatrix& m){
        if(m.getSize()==1){
            return m;
        }

        int len = m.getSize();
        std::cout << "##Called inner method with size==" << m.getSize() << "##";
        SquareMatrix A = m.subset(0, 0, len/2);
        std::cout << "\n\n Subsetting matrix - A:\n"<< A;
        SquareMatrix B = m.subset(0, len/2, len/2);
        std::cout << "\n\n Subsetting matrix - b:\n"<< B;
        SquareMatrix C = m.subset(len/2, len/2, len/2);
        std::cout << "\n\n Subsetting matrix - c:\n"<< C;

        SquareMatrix A_ = UTMatrixTransclose_inner(A);
        SquareMatrix B_ = UTMatrixTransclose_inner(B);


        SquareMatrix D = strassen(A_,C);
        std::cout << "\n\n matrix d-1:\n" << D;
        D = strassen(D,B_);
        std::cout << "\n\n matrix d-2:\n" << D;
        D.to_binary();
        std::cout << "\n\n matrix d after bin:\n" << D;

        m.replace_submatrix(A_, 0, 0);
        std::cout << "\n\n first replacement:\n"<< A_ << "\n" <<m;
        m.replace_submatrix(B_, 0, len/2);
        std::cout << "\n\n second replacement:\n"<< B_ << "\n" << m;
        m.replace_submatrix(D, len/2, len/2);
        std::cout << "\n\n third replacement:\n"<< D << "\n" << m;

        return m;

    }


    SquareMatrix UTMatrix_Transclose(){
        std::cout << "STARTING UTMATRIX";
        SquareMatrix m {getAdjMatrix()};

        std::cout << "\nStarting matrix\n" << m << "\n\n"; 

        double intpart;
        double decpart {modf(std::log2(m.getSize()),&intpart)};
        int orig_len = m.getSize();
        if (decpart!=0.0){
            m.addRowCol(pow(2,static_cast<int>(intpart)+1)-m.getSize());
        }

        std::cout << "Matrix after adding rowcols\n" << m;

        UTMatrixTransclose_inner(m);

        if(orig_len!=m.getSize()){
            m.remRowCol(m.getSize()-orig_len);
        }
        return m;
    }

public:
    void order(Vector<int>& ordering, bool desc = false){
        if(desc){
            adjList.sortBy(ordering, std::less<int>{});
        }else{
            adjList.sortBy(ordering);
        }
        //Re-map edges based on new ordering:
        for(int i=0;i<getNodeSize();++i){
            LList<int> newEdgeList {};
            for(auto v : getEdges(i)){
                newEdgeList.enqueue(ordering[v]);
            }
            adjList[i] = newEdgeList;
        }
    }



    //SquareMatrix fisherMayer(){
    SquareMatrix fisherMayer(){
        LList<LList<int>> sccs = Tarjan_SCC();

        Graph coll = collapse(sccs);

        Vector<int> topSort = coll.DFS().second;

        coll.order(topSort);

        //std::cout << "Topologycally sorted graph" << coll;

        SquareMatrix m = coll.UTMatrix_Transclose();

        return m;

        
    }


};

std::ostream& operator<<(std::ostream& os, const Graph& g) {
    os << "\n";

    for(int i=0; i<g.getNodeSize(); ++i){
        os << "Vertex " << i << ":  " <<g.getEdges(i);
    }

	os << std::endl;
    return os;
}

std::ostream& operator<<(std::ostream& os, const Color c){
    switch(c){
        case (Color::white): return os << "white"; break;
        case (Color::gray): return os << "gray"; break;
        case (Color::black): return os << "black"; break;
    }
}


std::ostream& operator<<(std::ostream& os, const Graph::Node n){
    std::string dist = std::to_string(n.d);
    if(dist=="-1"){
        dist = "INF";
    }
    std::string par = std::to_string(n.p);
    if(par=="-1"){
        par = "N";
    }
    std::string low = std::to_string(n.l);
    if(low=="-1"){
        low = "INF";
    }
    os << "{Color: " << n.c << ", Distance: " << dist << ", Parent: " << par << ", Time: " << std::to_string(n.f) << ", Lowlink: " << low << "}\n";
    return os;
}

#endif