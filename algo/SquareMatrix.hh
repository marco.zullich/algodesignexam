#ifndef SQUAREMHH
#define SQUAREMHH
/*
This header inherits from Matrix for the special case of a square matrix
with integer elements.
*/
#include "Matrix.h"

class SquareMatrix : public Matrix<int>{
public:
    SquareMatrix(unsigned int len, int default_elem = 0): Matrix<int> (len, len, default_elem) {}

    SquareMatrix(const Matrix<int>& m): Matrix<int> (m) {
        if(m.getCol()!=m.getRow()){
            throw std::runtime_error("Couldn't build Square Matrix out of Matrix, col and row size differ");
        }
    }

    //add general method for returning size of either col or row, adds up to getRow() and getCol()
    const unsigned int getSize(){
        return r;
    }

    //overwrite base class' method
    void addRowCol(unsigned int addrows, unsigned int addcols=0){
        if(addcols==0){
            addcols=addrows;
        }
        if(addrows!=addcols){
            throw std::runtime_error("Number of rows to add must equal number of columns to add");
        }
        Matrix<int>::addRowCol(addrows,addcols);
    }

    void remRowCol(unsigned int remrows, unsigned int remcols=0){
        if(remcols==0){
            remcols=remrows;
        }
        if(remrows!=remcols){
            throw std::runtime_error("Number of rows to remove must equal number of columns to add");
        }
        Matrix<int>::remRowCol(remrows,remcols);
    }

    void to_binary(){
        for(int i=0; i<elem.size(); ++i){
            if(elem[i]!=0){
                elem[i]=1;
            }
        }
    }

    SquareMatrix subset(unsigned int beginR, unsigned int beginC, unsigned int len){
        if(beginR+len > getSize() | beginC+len > getSize()){
            throw std::runtime_error("Matrix subsetting out of row or column bounds");
        }
        SquareMatrix s {len};
        for(int i=0;i<s.getSize();++i){
            for(int j=0;j<s.getSize();++j){
                s(i,j) = operator()(i+beginR,j+beginC);
            }
        }
        return s;
    }

    SquareMatrix replace_submatrix(SquareMatrix s, unsigned int beginR, unsigned int beginC){
        if(s.getSize()>getSize()){
            throw std::runtime_error("Submatrix larger than matrix: replacement impossible");
        }
        if(beginR+s.getSize() > getSize() | beginC+s.getSize()> getSize()){
            throw std::runtime_error("Index for replacement too big for submatrix to replace");
        }
        for(int i=0;i<s.getSize();++i){
            for(int j=0;j<s.getSize();++j){
                operator()(i+beginR,j+beginC) = s(i,j);
            }
        }
    }



    

};

#endif