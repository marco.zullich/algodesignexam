#include <iostream>
#include "SquareMatrix.hh"
#include "strassen.hh"

Matrix<int> dummy(){
    return Matrix<int> (5,3,2);
}

int main(){
    SquareMatrix a {5,2};
    a(4,4)=0.4;
    SquareMatrix b {4,3};
    b.addRowCol(1);
    b(1,3)=-2.4;
    b(4,3)=1;
    std::cout<<"a\n"<<a;
    std::cout<<"b\n"<<b;
    /*std::cout<<"b expanded\n"<<b;
    SquareMatrix c = add(a,b);
    std::cout<<"sum of matrices\n"<<c;
    SquareMatrix d = subtract(a,b);
    std::cout<<"subtraction of matrices\n"<<d;*/
    SquareMatrix e = strassen(a,b);
    std::cout<<"multiplication Strassen\n"<<e;
    SquareMatrix f = multiplyNaive(a,b);
    std::cout<<"multiplication naive\n"<<f;
}