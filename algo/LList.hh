#ifndef LISTHH
#define LISTHH

#include <iostream>
#include<stdexcept>
#include "Vector.hh"
/*
Singly linked list thought to be used as a queue or a priority queue
(not necessarily optimized for that final task)

Best implementation would've been
* Singly linked list
* Priority Queue inheriting from S.L.L.

*/

template<typename T>
class LList{
protected:
    struct Node{
        T val;
        Node* next;
        Node(const T& t): val{t}, next{nullptr} {}
        ~Node() {delete next;}
    };

    Node* head;
    //s keeps track of the size
    unsigned int s;

public:
    LList(): head{nullptr}, s{0} {}
    ~LList() {
        delete head;
    }

    LList(const LList& l): head{nullptr}, s{0} {
        for(auto i:l){
            enqueue(i);
        }
    }

    LList(LList&& l): head{nullptr}, s{0} {
        head = l.head;
        l.head = nullptr;
    }

    LList& operator=(const LList& l){
        delete head;
        head = nullptr;
        for(auto i:l){
            enqueue(i);
        }
        s = l.s;
    }

    LList& operator=(const LList&& l) = delete;

    //Needed primarily to work with Vector<LList<>>
    bool operator==(const LList<T> l){
        if(l.s != s){
            return false;
        }
        Node* temp1 {head};
        Node* temp2 {l.head};
        while(temp1!=nullptr){
            if(temp1->val!=temp2->val){
                return false;
            }
            temp1 = temp1->next;
            temp2 = temp2->next;
        }
        return true;
    }

    //equivalent of push back
    //addIfIn - if false doesn't perform enqueueing if there exists an element with the same value
    //returns true if enqueueing was performed, false otherwise
    bool enqueue(const T& t, bool addIfIn = true){
        if (head==nullptr){
            head = new Node(t);
            ++s;
            return true;
        }
        Node* temp{head};
        if(!addIfIn){
            if(temp->val==t){
                return false;
            }
        }
        while(temp->next != nullptr){
            if(!addIfIn){
                if(temp->val==t){
                    return false;
                }
            }
            
            temp = temp->next;
        }
        temp->next = new Node(t);
        ++s;
    }

    //pushes the element to the front of the list
    void push_front(const T& t){
        if (head==nullptr){
            head = new Node(t);
            ++s;
            return;
        }
        Node* temp{head};
        head = new Node(t);
        head -> next = temp;
        ++s;
    }

    //insert the element in a given point
    void push_before(const T& t, unsigned int i){
        if(i>getSize()){
            throw std::runtime_error("Index out of bounds on point insertion");
        }
        if(i==0){
            push_front(t); return;
        }
        if(i==getSize()){
            enqueue(t); return;
        }
        unsigned int j=0;
        Node* temp{head->next};
        Node* prev{head};
        while(j<i-1){
            prev = prev->next;
            temp = temp->next;
            ++j;
        }
        prev->next = new Node(t);
        prev = prev->next;
        prev->next = temp;
        ++s;
    }

    //performes insertion wrt the given ordering fct
    //when it finds an element which is (by default) larger than a given element
    //it inserts the element before that element - useful for the priority q.
    void insert_as_heap(const T& t, std::function<bool (T,T)> comp = std::less<T>{}){
        if(isEmpty()){
            enqueue(t);
            return;
        }

        unsigned int i=0;
        for(auto v : *this){
            if(comp(t,v)){
                push_before(t,i);
                return;
            }
            ++i;
        }
        enqueue(t);
    }

    //updates the queue by changing a value to a given element and performing re-insertion
    /*
    Note: a better implementation would remove only if the queue ordering is modified.
    */
    bool update(const T& t, const T& newt){
        bool exists_elem = remove(t);
        if(exists_elem){
            insert_as_heap(newt); return true;
        }
        return false;
    }


protected:
    //finds with the possibility to remove - protected because the user should use
    //* find(t, false) to find
    //* remove(t) to remove
    unsigned int find(const T& t, bool remove){
        if(isEmpty()){
            return -1;
        }
        unsigned int i{0};
        Node* temp {head};
        Node* prev {nullptr};

        while(temp!=nullptr){
            if(temp->val==t){
                if(remove){
                    if(temp==head){
                        dequeue();
                    }else{
                        prev->next = temp->next;
                        //delete temp;
                        --s;
                    }
                    //default return if elimination went right -> -2;
                    return -2;
                }
                return i;
            }
            ++i;
            prev = temp;
            temp = temp->next;

        }
        return -1;
    }

public:
    //user-safe version of find
    unsigned int find(const T&t){
        return find(t,false);
    }

    //user-safe wrapper for removal
    bool remove(const T& t){
        return find(t,true)==-2;
    }


    //frees first item on queue
    T dequeue(){
        if(isEmpty()){
            throw std::runtime_error("Cannot dequeue from an empty list");
        }
        T temp{head->val};
        head = head -> next;
        --s;
        return temp;
    }

    T getHead(){
        return head->val;
    }

    bool isEmpty(){
        return s==0;
    }


    unsigned int getSize(){return s;}

    //utility fct - transforms queue to a vector
    Vector<T> toVector(){
        Vector<T> v {s};
        Node* temp {head};
        unsigned int i {0};
        while(temp!=nullptr){
            v[i] = temp->val;
            ++i;
            temp = temp->next;
        }
        return v;
    }

    //iterator part
    class Iterator{
        using Node = LList<T>::Node;
        Node* current;

    public:
        Iterator(Node* n): current{n} {}
        T& operator*() const {return current->val;}

        Iterator& operator++(){
            current=current->next;
            return *this;
        }

        Iterator& operator++(int){
            Iterator it{current};
            ++(*this);
            return it;
        }

        bool operator==(const Iterator& other) {
            return this->current == other.current;
        }

        bool operator!=(const Iterator& other) { return !(*this == other); }
    };

    Iterator begin() {return Iterator(head);}
    Iterator end() {return Iterator(nullptr);}


    class ConstIterator : public Iterator{
        using parent = Iterator;

    public:
        using parent::Iterator;
        const T& operator*() const { return parent::operator*(); }
    };

    ConstIterator begin() const { return ConstIterator{head}; }
    ConstIterator end() const { return ConstIterator{nullptr}; }

};

template <typename T>
std::ostream& operator<<(std::ostream& os, const LList<T>& l) {
    os << "\n";
    typename LList<T>::ConstIterator i = l.begin();

    while(i != l.end()){
        os << *i << " -> ";
        ++i;
    }


	
	os << "##END##" << std::endl;
  return os;
}

#endif