#include "Wgraph.hh"

int main(){
    using Edge = Wgraph::Edge;
    using Node = Wgraph::Node;
    Vector<LList<Edge>> v {5};
    v[0].enqueue(Edge(1,3));
    v[0].enqueue(Edge(2,1));
    v[0].enqueue(Edge(3,4));
    v[1].enqueue(Edge(4,2));
    v[2].enqueue(Edge(3,7));
    v[2].enqueue(Edge(4,15));
    v[3].enqueue(Edge(4,2));

    Wgraph w {v};

    std::cout << w;

    LList<int> d = w.dijkstra(0,4);

    std::cout << d;

    SquareMatrix W = w.floydWarshall();

    std::cout << W;
}