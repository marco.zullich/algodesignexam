#ifndef WGRAPHHH
#define WGRAPHHH

#include "LList.hh"
#include "Vector.hh"
#include "SquareMatrix.hh"

//Weighted graph
/*
Note: it's thought to work only with positive integer weights
infinite weight is hence put to -1, with need to define operations 
* min
* sum
on weights - see later
*/

class Wgraph{
public:
    struct Node{
        int i;
        int d;
        int p;
        //Note:index=0 defined only to allow for a default constructor.
        Node(int index=0, int dist=-1, int parent=-1) : i{index}, d{dist}, p{parent} {}
        Node(const Node& n): i{n.i}, d{n.d}, p{n.p} {}
        Node(const Node&& n): i{n.i}, d{n.d}, p{n.p} {}

        Node& operator=(const Node& n){
            i=n.i; d=n.d; p=n.p;
        }
        //define comparison operators for node

        bool operator>(const Node& n) const{
            if(n.d==-1){return false;}
            if(d==-1 & n.d>=0){return true;}
            return n>n.d;
        }

        bool operator==(const Node& n) const{
            return d==n.d;
        }

        bool operator<(const Node& n) const {
            if(d==-1){return false;}
            if(n.d==-1 & d>=0){return true;}
            return d<n.d;
        }


        
    };

    //edges now need a structure, since they need destination (to) and weight
    struct Edge{
        int to;
        int w;
        Edge(int destination, int weight=1): to{destination}, w{weight} {}

        bool operator>(const Edge& e) const {
            return w>e.w;
        }

        bool operator==(const Edge& e) const{
            return w==e.w;
        }

        bool operator<(const Edge& e) const{
            return w<e.w;
        }
    };
protected: //only attribute is the adj list as vector of list of EDGES
    Vector<LList<Edge>> adl;
public:
    Wgraph(Vector<LList<Edge>> adjList): adl{adjList} {}

    const unsigned int getNodeSize() const {return adl.size();}

    const LList<Edge> getEdges (const unsigned int i) const {return adl[i];}

    //rebuilds the path obtained via dijkstra or A* from start to end of the list
private:
    LList<int> RebuildPath(LList<Node> set){
        //to allow backtracking, transform the set to Vector
        Vector<Node> set_ {set.toVector()};
        Node start {set_[0]};

        int tempsize = set_.size()-1;
        Node temp {set_[tempsize]};

        LList<int> path {};
        path.push_front(temp.i);

        while(!(temp==start)){
            for(int i=tempsize;i>=0;--i){
                if(temp.p == set_[i].i){
                    path.push_front(set_[i].i);
                    temp = set_[i];
                    tempsize = i;
                    break;
                }
            }
        }
        return path;
    }

public:
    //helper for floyd Warshall
    SquareMatrix getWeightsMatrix(){
        SquareMatrix W {getNodeSize(), -1}; //I use -1 as infty since weights should all be positive  
        for(int i=0;i<getNodeSize();++i){
            W(i,i)=0; //update diag weight to 0
            for(auto v : getEdges(i)){
                W(i,v.to)=v.w;
            }
        }
        return W;
    }

    LList<int> dijkstra(int start, int end){
        if(start<0 | start>=getNodeSize()){
            throw std::runtime_error("'Start' index out of bounds");
        }

        //Define all nodes
        Vector<Node> nodes {getNodeSize()};
        for(int i=0;i<getNodeSize();++i){
            nodes[i].i=i;
        }

        //set first distance to 0;
        nodes[start].d = 0;

        //instantiate set (linked list)
        LList<Node> S {};

        //instantiate priority queue - priority is determined by the smallest d number of the Node
        LList<Node> Q {};
        for(int i=0;i<nodes.size();++i){
            Q.insert_as_heap(nodes[i]);
        }

        while(!Q.isEmpty()){
            //get the u node from the top of the priority list
            Node u = Q.dequeue();
            //insert it into the set;
            S.enqueue(u,false);

            if(u.i==end){
                return RebuildPath(S);
            }
            //relax all nodes in the adjacency list of u
            for(auto v : getEdges(u.i)){
                //RELAX:
                if(nodes[v.to]>Node(0,u.d+v.w,-1)){
                    Node temp = nodes[v.to];
                    nodes[v.to].d = u.d+v.w;
                    nodes[v.to].p = u.i;
                    Q.update(temp, nodes[v.to]);
                }
            }
        }

        //No path found - return empty list
        return LList<int>{};

        
    }

    


protected: //other helpers for floyd Warshall since we use -1 as infinite weight
    int getMinWeights(int w1, int w2){
        if(w1==-1 | w1==w2){return w2;}
        if(w2==-1){return w1;}
        else{return std::min(w1,w2);}
    }

    int sumWeights(int w1, int w2){
        if(w1==-1 | w2==-1){return -1;}
        else{return w1+w2;}
    }

public:
    SquareMatrix floydWarshall(){
        SquareMatrix W = getWeightsMatrix(); //D(-1)
        SquareMatrix D = SquareMatrix{getNodeSize()}; //D(0)
        for(int k=0; k<getNodeSize(); ++k){
            for(int i=0; i<getNodeSize(); ++i){
                for(int j=0; j<getNodeSize(); ++j){
                    D(i,j) = getMinWeights(W(i,j), sumWeights(W(i,k),W(k,j)));
                }
            }
            //in order not to waste memory, D(k) overwrites W, D(k+1) overwrites D(k)
            W = D;
            D = SquareMatrix{getNodeSize()};
        }
        return W;
    }

    LList<int> aplus(int start, int end, int(*heuristic)(Node n1, Node n2) ){
        if(start<0 | start>=getNodeSize() | end<0 | end>=getNodeSize()){
            throw std::runtime_error("'Start' index out of bounds");
        }

        //Define all nodes
        Vector<Node> nodes {getNodeSize()};
        for(int i=0;i<getNodeSize();++i){
            nodes[i].i=i;
        }

        //set first distance to 0;
        nodes[start].d = 0;
        nodes[start].p = start;

        //instantiate set (linked list)
        LList<Node> S {};

        //instantiate priority queue - priority is determined by the smallest d number of the Node
        LList<Node> Q {};
        for(int i=0;i<nodes.size();++i){
            Q.insert_as_heap(nodes[i]);
        }

        while(!Q.isEmpty()){
            Node z = Q.dequeue();
            S.enqueue(z);

            if(z.i==end){
                return RebuildPath(S);
            }

            for(auto v : getEdges(z.i)){
                if(nodes[v.to] > Node(0,z.d+v.w,-1)){
                    Node before = nodes[v.to];
                    nodes[v.to].p = z.i;
                    nodes[v.to].d = z.d + v.w;
                    Node after = nodes[v.to];
                    after.d += heuristic(after,z);
                    Q.update(before, after);
                }
            }
        }
    


    }






};

std::ostream& operator<<(std::ostream& os, const Wgraph::Node n){
    std::string dist = std::to_string(n.d);
    if(dist=="-1"){
        dist = "INF";
    }
    std::string par = std::to_string(n.p);
    if(par=="-1"){
        par = "NULL";
    }

    os << "{" << n.i << ", p=" << par << ", d=" << dist << "}";

    return os;
}

std::ostream& operator<<(std::ostream& os, const Wgraph::Edge e){
    os << "(" << e.to << ", " << e.w << ") ";
}

std::ostream& operator<<(std::ostream& os, const Wgraph g){
    for(int i=0;i<g.getNodeSize();++i){
        os << "Node " << std::to_string(i) << ":\n";
        for(auto j : g.getEdges(i)){
            os << j;
        }
        os << "\n";
    }
    return os;
}

#endif