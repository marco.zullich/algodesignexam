#include "Vector.hh"
#include "LList.hh"

Vector<int> ComputePrefixFunction(std::string P){
    int m = P.length();

    Vector<int> pi {m,-1};

    int k = -1;

    for(int q=1; q<m; ++q){
        while(k>=0 & P[k+1]!=P[q]){
            k = pi[k];
        }
        if(P[k+1]==P[q]){
            ++k;
        }
        pi[q]=k;
    }

    return pi;
}


LList<int> KMP_Matcher(std::string T, std::string P){
    if(T.length()<P.length()){
        return LList<int>{};
    }

    LList<int> shift{};

    int n = T.length();
    int m = P.length();
    Vector<int> pi = ComputePrefixFunction(P);

    int q=-1;

    for(int i=0;i<n;++i){
        while(q>=0 & P[q+1]!=T[i]){
            q = pi[q];
        }
        if(P[q+1]==T[i]){
            ++q;
        }
        if(q==m-1){
            shift.enqueue(i-m+1);
            q = pi[q];
        }
    }
    
    return shift;
}

//for testing purposes
int main(){
    std::string text {"cccciiiccc ccccccc"};
    std::string pattern {"ccc"};

    LList<int> pattern_in_text = KMP_Matcher(text,pattern);

    std::cout << "\n'" << pattern <<"' matches '" << text << "' with shifts:" << pattern_in_text;
}

