#include "Graph.hh"


int main(){
    
    Vector<LList<int>> a{5};

    a[0].enqueue(1);
    a[0].enqueue(2);
    a[1].enqueue(2);
    a[2].enqueue(4);
    a[4].enqueue(3);
    a[4].enqueue(1);
    //a[3].enqueue(0);
    

    /* Vector<LList<int>> a{2};
    a[0].enqueue(1);
    a[1].enqueue(0);
    */
    Graph g {a};

    
    std::cout << "PRINTING GRAPH g " << g;

    
    LList<LList<int>> l {g.Tarjan_SCC()};

    std::cout << "Strongly connected components\n" <<  l;
    
    /*
    SquareMatrix adjm = g.getAdjMatrix();

    std::cout << "ADJ MATRIX\n" << adjm;
    */

    Graph collapsed = g.collapse(g.Tarjan_SCC());

    std::cout << "Collapsed graph\n" << collapsed;

    //std::pair<Vector<Graph::Node>,Vector<int>> dfs = collapsed.DFS();

    //std::cout << "DFS\n" << dfs.first;

    //std::cout << "TOPSORT\n" << dfs.second;

    //SquareMatrix adjm = collapsed.getAdjMatrix();

    //std::cout << "ADJM for collapsed\n" << adjm;

    SquareMatrix fm = g.fisherMayer();

    std::cout << "ADJM fm\n" << fm;

    //Vector<int> topsort = collapsed.DFS().second;
    //collapsed.order(topsort);

    //std::cout << "g collapsed and ordered"<< collapsed;


    return 1;
}