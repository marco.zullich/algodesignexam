#ifndef MATRIXH
#define MATRIXH

/*
This container wraps a Vector such that it can be used as a matrix
Has no push_back or resize, only access to existing memory via a (,) operator
*/

#include<iostream>
#include<stdexcept>
#include "Vector.hh"

template<typename T>

class Matrix{
protected:
    int r;
    int c;
    Vector<T> elem;

public:
    Matrix(unsigned int row=0, unsigned int col=0, T default_elem= T{}): r{row}, c{col}, elem{Vector<T>(row*col, default_elem)} { }

    Matrix(const Matrix& m): r{m.getRow()}, c{m.getCol()}, elem{m.elem} {}
    Matrix& operator=(const Matrix& m){
        r = m.getRow();
        c = m.getCol();
        elem = m.elem;
    }
    Matrix(const Matrix&& m): r{m.getRow()}, c{m.getCol()}, elem{std::move(m.elem)} {}
    Matrix&& operator==(const Matrix&& m){
        r = m.getRow();
        c = m.getCol();
        elem = std::move(m.elem);
    }

    T& operator()(unsigned int row, unsigned int col){return elem[rangecheckRow(row)*c + rangecheckCol(col)];}
    const T& operator()(unsigned int row, unsigned int col) const {return elem[rangecheckRow(row)*c + rangecheckCol(col)];}


    const unsigned int getRow() const {return r;}
    const unsigned int getCol() const {return c;}

    //checks if the row or col values passed by the users are within the range
    const unsigned int rangecheckRow(const unsigned int i) const{
        if(i<r) return i;
        throw std::runtime_error ("Row index out of range");
    }

    const unsigned int rangecheckCol(const unsigned int i) const{
        if(i<c) return i;
        throw std::runtime_error ("Column index out of range");
    }

    //this next method lets us add a specified number of rows and columns to the matrix
    void addRowCol(unsigned int addrows, unsigned int addcols){
        //create new element Vector
        Vector<T> elem_new((r+addrows)*(c+addcols));
        //populate the vector with the old elements; otherwise set everything to 0 (already set by default!)
        for(auto i=0; i<r; ++i){
            for(auto j=0; j<c; ++j){
                elem_new[i*(c+addcols) + j]=elem[i*c+j];
            }
        }
        //move elem_new to elem
        elem = std::move(elem_new);

        //adjust new row and col length
        r = r+addrows;
        c = c+addcols;
    }


    //like above - but removes a specified no. of rows and cols
    void remRowCol(unsigned int remrows, unsigned int remcols){
        if(r-remrows<=0 | c-remcols<=0){
            throw std::runtime_error("Number of rows or cols to remove too high");
        }
        Vector<T> elem_new((r-remrows)*(c-remcols));
        for(auto i=0;i<r-remrows;++i){
            for(auto j=0;j<c-remcols;++j){
                elem_new[i*(c-remcols) + j]=elem[i*c+j];
            }
        }
        elem = std::move(elem_new);

        r = r-remrows;
        c = c-remcols;
    }

    
};

//utility functions - addition of two matricis (might've done operator+ as well...)
template <typename T>
Matrix<T> add(Matrix<T>& A, Matrix<T>& B,int beginRA=0, int beginCA=0, int beginRB=0, int beginCB=0, int len=0){
    if(A.getCol()!=B.getCol() | A.getRow()!=B.getRow()){
        throw std::runtime_error("Addition of matrices permitted only if they share dimensions");
    }
    if(len==0){
        len=A.getCol();
    }
    Matrix<T> sum {len, len, T{}};
    for(int i=0;i<len;++i){
        for(int j=0;j<len;++j){
            sum(i,j) = A(beginRA+i,beginCA+j)+B(beginRB+i,beginCB+j);
        }
    }
    return sum;
}

template <typename T>
Matrix<T> subtract(Matrix<T>& A, Matrix<T>& B,int beginRA=0, int beginCA=0, int beginRB=0, int beginCB=0, int len=0){
    if(A.getCol()!=B.getCol() | A.getRow()!=B.getRow()){
        throw std::runtime_error("Addition of matrices permitted only if they share dimensions");
    }
    if(len==0){
        len=A.getCol();
    }
    Matrix<T> subt {len, len, T{}};
    for(int i=0;i<len;++i){
        for(int j=0;j<len;++j){
            subt(i,j) = A(beginRA+i,beginCA+j)-B(beginRB+i,beginCB+j);
        }
    }
    return subt;
}


//printing fct
template <typename T>
std::ostream& operator<<(std::ostream& os, const Matrix<T>& mat) {
    
    
    for (unsigned int i=0; i< mat.getRow(); ++i){
        os << "|";
        for(unsigned int j=0; j< mat.getCol(); ++j){
            os << mat(i,j) << ", ";
        }
        os << "\b\b|\n";
    }
    os << "\n";
    
    return os;
}

#endif